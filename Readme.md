## LedStrip

This Project contains the code for a custom build led strip controlled by
an Arduino ligthing some stone salt crystals.

There is a Qt App for programing the led strip over a serial interface.

I also wrote
[a post about this project on my blog](https://blog.kalehmann.de/blog/2019/02/19/crystal-led-strip.html)
(attention, German).

### Contents

- [The Arduino project](LedStrip)
- [The Qt app](LedApp)
- [The Qt extension for communication with the arduino](LedApp/QmlQLedPlugin)
- [The documentation for the serial api](Api.md)
- [A fritzing project for the led strip](LedStrip.fzz)
