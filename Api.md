# LedStrip Api

## Preface

The intention of the api is to leave no necissity for mainting a state.
Every data package should be complete without any additional information or 
additional packages. Every package is a single line terminated by a line feed
(0xa).

Lines that could not be understood should be ignored silently.

## Available commands

| COMMAND                   | DESCRIPTION                             | EXAMPLE RESPONSE                            |
|:--------------------------|:----------------------------------------|:--------------------------------------------|
| `SET LED [LED]#[COLOR]\n` | Sets the color of the given led         | ``                                          |
| `GET ALL\n`               | Get the current colors of all leds      | `LED 0#0A0000 1#FF0000 2#00FF00 3#0000FF\n` || `GET LED [LED]\n`         | Get the color of the given led          | `LED 1#FF0000\n`                            | | `WHAT ARE YOU?\n`         | Can be used to test the connection      | `I AM A LEDSTRIP\n`                         |