import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.0
import LedStrip 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("LED APP")
    id: appWindow

    LedStrip {
        id: ledStrip
    }

    TabBar {
        id: bar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Connection")
            width: implicitWidth
        }

        TabButton {
            text: qsTr("LED Effects")
            width: implicitWidth
        }
    }

    SwipeView {
        id: swipeView
        currentIndex: bar.currentIndex
        anchors.top: bar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: pageIndicator.top

        ConnectionView {}

        LedView {
            id: ledView
        }
    }

    PageIndicator {
        id: pageIndicator
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        currentIndex: swipeView.currentIndex
        count: swipeView.count
    }
}
