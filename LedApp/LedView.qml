import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Item {
    GridLayout {
        id: grid
        columns: 2
        anchors.fill: parent

        Switch {
            id: flipSwitch
        }

        Leds {
            id: leds
            flipped: flipSwitch.checked
            ledColors: ledStrip.ledColors
        }

        StackLayout  {
            id: swipeView
            currentIndex: tabBar.currentIndex
            width: parent.width
            height: 300
            Layout.columnSpan: 2

            StaticPicker {
                id: staticPicker
                ledColors: ledStrip.ledColors
                checkedLed: leds.checkedLed
                Connections {
                    target: ledStrip
                    onLedStripConnectedChanged: staticPicker.connectionChanged()
                }
            }

            RainbowPicker {
                id: rainbowPicker
                ledColors: ledStrip.ledColors
                flipped: flipSwitch.checked
            }

            PulsatePicker {
                id: pulsatePicker
                ledColors: ledStrip.ledColors
                flipped: flipSwitch.checked
            }

            KnightRiderPicker {
                id: knightRiderPicker
                ledColors: ledStrip.ledColors
                flipped: flipSwitch.checked
            }
         }

        TabBar {
            id: tabBar
            currentIndex: swipeView.currentIndex
            anchors.bottom: appWindow.bottom
            Layout.columnSpan: 2
            TabButton {
                text: qsTr("Static")
                width: grid.width / 4
            }

            TabButton {
                text: qsTr("Rainbow")
                width: grid.width / 4
            }

            TabButton {
                text: qsTr("Pulsate")
                width: grid.width / 4
            }

            TabButton {
                text: qsTr("Knight Rider")
                width: grid.width / 4
            }

            onCurrentIndexChanged: {
                switch (currentIndex) {
                case  0:
                    leds.selectable = true;
                    rainbowPicker.active = false;
                    pulsatePicker.active = false;
                    knightRiderPicker.active = false;
                    break;
                case 1:
                    leds.selectable = false;
                    rainbowPicker.active = true;
                    pulsatePicker.active = false;
                    knightRiderPicker.active = false;
                    break;
                case  2:
                    leds.selectable = false;
                    rainbowPicker.active = false;
                    pulsatePicker.active = true;
                    knightRiderPicker.active = false;
                    break;
                case  3:
                    leds.selectable = false;
                    rainbowPicker.active = false;
                    pulsatePicker.active = false;
                    knightRiderPicker.active = true;
                    break;
                }
            }
        }
    }
}
