import QtQuick 2.0
import QtQuick.Controls 2.5

Item {
    id: pulsatePicker
    property var ledColors
    property bool active
    property bool flipped
    property list<Slider> sliders
    property int value
    property int valueOffset: 0

    Component.onCompleted: {
        pulsatePicker.sliders.push(led0Value);
        pulsatePicker.sliders.push(led1Value);
        pulsatePicker.sliders.push(led2Value);
        pulsatePicker.sliders.push(led3Value);
    }

    onValueChanged: {
        for (var i=0; i<4; i++) {
            colorWrapper.color = ledStrip.ledColors[i];
            colorWrapper.color.hsvHue = colorSlider.value / 255;
            var minVal = pulsatePicker.sliders[i].first.value;
            var maxVal = pulsatePicker.sliders[i].second.value;
            var range = maxVal - minVal;
            var tmpVal = (value + valueOffset) % 512;
            var multiplier = (tmpVal > 255 ? 511 - tmpVal : tmpVal ) / 255;
            colorWrapper.color.hsvValue = (multiplier * range + minVal) / 255;
            colorWrapper.color.hsvSaturation = saturationSlider.value / 255;
            var led = flipped ? 3 - i : i;
            pulsatePicker.ledColors[led] = colorWrapper.color;
        }
    }

    onFlippedChanged: {
        var values = [];
        for (var i=0; i<4; i++) {
            values.push([sliders[i].first.value, sliders[i].second.value]);
        }
        for (i=3; i>=0; i--) {
            pulsatePicker.sliders[i].first.value = values[3 - i][0];
            pulsatePicker.sliders[i].second.value = values[3 - i][1];
        }
    }

    SequentialAnimation {
        id: anim
        running: pulsatePicker.active
        loops: Animation.Infinite

        NumberAnimation {
            id: anim1
            target: pulsatePicker
            property: "value"
            duration: speedSlider.value
            easing.type: Easing.InOutQuad
            from: 0
            to: 511
        }
    }

    Item {
        id: colorWrapper
        property color color
    }

    RangeSlider {
        id: led0Value
        orientation: Qt.Vertical
        from: 0
        first.value: 10
        second.value: 200
        to: 255
        x: 120
        height: 150
    }

    RangeSlider {
        id: led1Value
        orientation: Qt.Vertical
        from: 0
        first.value: 10
        second.value: 200
        to: 255
        x: 240
        height: led0Value.height
    }

    RangeSlider {
        id: led2Value
        orientation: Qt.Vertical
        from: 0
        first.value: 10
        second.value: 200
        to: 255
        x: 360
        height: led0Value.height
    }

    RangeSlider {
        id: led3Value
        orientation: Qt.Vertical
        from: 0
        first.value: 10
        second.value: 200
        to: 255
        x: 480
        height: led0Value.height
    }


    Text {
        text: qsTr("Speed")
        anchors.right: speedSlider.left
        anchors.verticalCenter: speedSlider.verticalCenter
    }

    Slider {
        id: speedSlider
        anchors.top:  led0Value.bottom
        anchors.left: led0Value.left
        width: led3Value.x - led0Value.x
        from: 2000
        to: 10000
        onValueChanged: {
            if (anim.running) {
                anim.pause();
                pulsatePicker.valueOffset = (pulsatePicker.valueOffset + pulsatePicker.value) % 511;
                anim.stop();
                anim.start();
            }
        }
    }

    Text {
        text: qsTr("Color")
        anchors.right: colorSlider.left
        anchors.verticalCenter: colorSlider.verticalCenter
    }

    Slider {
        id: colorSlider
        anchors.top:  speedSlider.bottom
        anchors.left: speedSlider.left
        width: led3Value.x - led0Value.x
        from: 0
        to: 255
    }

    Text {
        text: qsTr("Saturation")
        anchors.right: saturationSlider.left
        anchors.verticalCenter: saturationSlider.verticalCenter
    }

    Slider {
        id: saturationSlider
        anchors.top:  colorSlider.bottom
        anchors.left: colorSlider.left
        width: led3Value.x - led0Value.x
        from: 0
        to: 255
    }
}
