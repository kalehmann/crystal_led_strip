import QtQuick 2.0
import QtQuick.Controls 2.5

Item {
    id: knightRiderPicker
    property bool active
    property bool flipped
    property var ledColors
    property int value
    property int valueOffset: 0
    property list<Slider> sliders

    Component.onCompleted: {
        knightRiderPicker.sliders.push(led0Value);
        knightRiderPicker.sliders.push(led1Value);
        knightRiderPicker.sliders.push(led2Value);
        knightRiderPicker.sliders.push(led3Value);
    }

    onValueChanged: {
        var led;
        var firstLed = Math.floor(((value + valueOffset) % 1023) / 256);
        var numLeds = widthSlider.value;
        var backward = directionSwitch.checked;

        for (var i = numLeds - 1; i>=0; i--) {
            led = backward ? 3 - (firstLed + i) % 4 : (firstLed + i) % 4;
            colorWrapper.color.hsvValue = sliders[flipped ? 3 - led : led].value / 255;
            colorWrapper.color.hsvSaturation = saturationSlider.value / 255
            colorWrapper.color.hsvHue = hueSlider.value / 255
            knightRiderPicker.ledColors[led] = colorWrapper.color;
        }

        colorWrapper.color.hsvValue = 0;

        for (i = numLeds; i < 4; i++) {
            led = backward ? 3 - (firstLed + i) % 4 : (firstLed + i) % 4;
             knightRiderPicker.ledColors[led] = colorWrapper.color;
        }
    }

    onFlippedChanged: {
        var values = [];
        for (var i=0; i<4; i++) {
            values.push(knightRiderPicker.sliders[i].value);
        }

        for (i=3; i>=0; i--) {
            knightRiderPicker.sliders[i].value = values[3 - i];
        }
    }

    SequentialAnimation {
        id: anim
        running: knightRiderPicker.active
        loops: Animation.Infinite

        NumberAnimation {
            id: anim1
            target: knightRiderPicker
            property: "value"
            duration: speedSlider.value
            easing.type: Easing.Linear
            from: 0
            to: 1023
        }
    }

    Item {
        id: colorWrapper
        property color color
    }

    Text {
        text:  qsTr("Direction")
        id: directionText
        anchors.top: led0Value.top
        x: 20
    }

    Switch {
        id: directionSwitch
        anchors.left: directionText.left
        anchors.top: directionText.bottom
    }

    Slider {
        id: led0Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 120
        height: 150
    }

    Slider {
        id: led1Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 240
        height: 150
    }

    Slider {
        id: led2Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 360
        height: 150
    }

    Slider {
        id: led3Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 480
        height: 150
    }

    Text {
        text: qsTr("Width")
        anchors.verticalCenter: widthSlider.verticalCenter
        anchors.right: widthSlider.left
        anchors.rightMargin: 20
    }

    Slider {
        id: widthSlider
        from: 1
        value: 1
        to: 3
        snapMode: Slider.SnapAlways
        stepSize: 1
        x: 80
        y: 170
    }

    Text {
        text: qsTr("Speed")
        anchors.verticalCenter: speedSlider.verticalCenter
        anchors.right: speedSlider.left
        anchors.rightMargin: 20
    }

    Slider {
        id: speedSlider
        from: 500
        to: 10000
        anchors.left: widthSlider.left
        anchors.top: widthSlider.bottom
        onValueChanged: {
            if (anim.running) {
                anim.pause();
                knightRiderPicker.valueOffset = (knightRiderPicker.valueOffset + knightRiderPicker.value) % 1023;
                anim.stop();
                anim.start();
            }
        }
    }

    Text {
        text: qsTr("Hue")
        anchors.verticalCenter: hueSlider.verticalCenter
        anchors.right: hueSlider.left
        anchors.rightMargin: 20
    }

    Slider {
        id: hueSlider
        from: 0
        value: 30
        to: 255
        anchors.left: widthSlider.right
        anchors.leftMargin: 80
        anchors.verticalCenter: widthSlider.verticalCenter
    }

    Text {
        text: qsTr("Saturation")
        anchors.verticalCenter: saturationSlider.verticalCenter
        anchors.right: saturationSlider.left
        anchors.rightMargin: 20
    }

    Slider {
        id: saturationSlider
        from: 0
        value: 30
        to: 255
        anchors.left: hueSlider.left
        anchors.top: hueSlider.bottom
    }
}
