import QtQuick 2.0
import QtQuick.Controls 2.5

Item {
    id: rainbowPicker
    property var ledColors
    property bool active
    property bool flipped
    property int hue
    property list<Slider> sliders
    property int hueOffset: 0

    Component.onCompleted: {
        rainbowPicker.sliders.push(led0Value);
        rainbowPicker.sliders.push(led1Value);
        rainbowPicker.sliders.push(led2Value);
        rainbowPicker.sliders.push(led3Value);
    }

    onHueChanged: {
        for (var i=0; i<4; i++) {
            colorWrapper.color = ledStrip.ledColors[i];
            colorWrapper.color.hsvHue = ((hue + hueOffset) % 256) / 255;
            colorWrapper.color.hsvSaturation = 1;
            colorWrapper.color.hsvValue = rainbowPicker.sliders[i].value / 255;
            var led = flipped ? 3 - i : i;
            rainbowPicker.ledColors[led] = colorWrapper.color;
        }
    }

    onFlippedChanged: {
        var values = [];
        for (var i=0; i<4; i++) {
            values.push(rainbowPicker.sliders[i].value);
        }

        for (i=3; i>=0; i--) {
            rainbowPicker.sliders[i].value = values[3 - i];
        }
    }

    SequentialAnimation {
        id: anim
        running: rainbowPicker.active
        loops: Animation.Infinite

        NumberAnimation {
            id: anim1
            target: rainbowPicker
            property: "hue"
            duration: speedSlider.value
            easing.type: Easing.InOutQuad
            from: 0
            to: 255
        }
    }

    Item {
        id: colorWrapper
        property color color
    }

    Text {
        text: qsTr("Speed")
        anchors.right: speedSlider.left
        anchors.verticalCenter: speedSlider.verticalCenter
    }

    Slider {
        id: led0Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 120
    }

    Slider {
        id: led1Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 240
    }

    Slider {
        id: led2Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 360
    }

    Slider {
        id: led3Value
        orientation: Qt.Vertical
        from: 0
        value: 10
        to: 255
        x: 480
    }

    Slider {
        id: speedSlider
        anchors.top:  led0Value.bottom
        anchors.left: led0Value.left
        width: led3Value.x - led0Value.x
        from: 2000
        to: 10000
        onValueChanged: {
            if (anim.running) {
                anim.pause();
                rainbowPicker.hueOffset = (rainbowPicker.hueOffset + rainbowPicker.hue) % 256;
                anim.stop();
                anim.start();
            }
        }
    }
}
