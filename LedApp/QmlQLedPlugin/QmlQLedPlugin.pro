TEMPLATE = lib
CONFIG += plugin
QT += qml quick serialport

DESTDIR = LedStrip
TARGET = ledplugin

OBJECTS_DIR = tmp
MOC_DIR = tmp

SOURCES += \
    ledplugin.cpp \
    ledstrip.cpp

HEADERS += \
    ledplugin.h \
    ledstrip.h \
    ledstriplog.h

DISTFILES += \
    LedStrip/qmldir \
    LedStrip/qmldir
