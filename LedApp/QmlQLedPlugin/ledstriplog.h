#ifndef LEDSTRIPLOG_H
#define LEDSTRIPLOG_H

#include <QObject>

class LedStripLog : public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY(QString message MEMBER message NOTIFY messageChanged)
    Q_PROPERTY(QString level MEMBER level NOTIFY levelChanged)

    QString message;
    QString level;

signals:
    void messageChanged();
    void levelChanged();
};

#endif // LEDSTRIPLOG_H
