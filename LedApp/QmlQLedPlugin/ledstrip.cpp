﻿#include "ledstrip.h"
#include <string.h>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>

LedStrip::LedStrip(QObject *parent): QObject(parent)
{
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updatePorts()));
    timer->start(100);

    connect(&this->_serial, SIGNAL(readyRead()), this, SLOT(serialReadyRead()));

    for (int i=0; i<NUM_LEDS; i++) {
        this->_ledColors.append(QString());
    }
}

QStringList LedStrip::availablePorts()
{
    return this->_availablePortNames;
}

QString LedStrip::getSerialPort()
{
    return this->_serial.portName();
}

void LedStrip::setSerialPort(QString name)
{
    this->_ledStripConnected = false;
    emit this->ledStripConnectedChanged();

    if (this->_serial.isOpen()) {
        this->_serial.close();
    }
    this->_serial.setPortName(name);
    this->_serial.open(QIODevice::ReadWrite);
    this->_serial.setBaudRate(QSerialPort::Baud9600);
    this->_serial.setDataBits(QSerialPort::Data8);
    this->_serial.setParity(QSerialPort::NoParity);
    this->_serial.setStopBits(QSerialPort::OneStop);
    this->_serial.setFlowControl(QSerialPort::NoFlowControl);

    if (this->_serial.isOpen() && this->_serial.isWritable()) {
        this->log(
            QString("Successfully connected to port " + name),
            QString("INFO")
        );
    } else {
        this->log(QString("Could not connect to port " + name), QString("ERROR"));
    }
    emit this->logChanged();
    emit this->serialPortChanged();

    QTimer::singleShot(2000, this, SLOT(testLedStripStatus()));
}

bool LedStrip::ledStripConnected()
{
    return this->_ledStripConnected;
}

QStringList LedStrip::getLedColors()
{
   return this->_ledColors;
}

void LedStrip::setLedColors(QStringList colors)
{
    if (!(this->_serial.isOpen() && this->_serial.isWritable())) {
        this->log("Port is not open for writing", "ERROR");
        return;
    }

    bool ledChanged = false;

    if (colors.length() != NUM_LEDS) {
        return;
    }

    for (int i = 0; i < colors.length(); i++) {
        if (this->_ledColors[i] != colors[i]) {
            ledChanged = true;
            this->_ledColors[i] = colors[i];
            QString command = QString("SET LED ");
            command.append('0' + i);
            command.append(this->_ledColors[i].toUpper());
            this->sendCommand(command);
        }
    }

    if (ledChanged) {
        emit this->ledColorsChanged();
    }
}

QVariant LedStrip::getLog()
{
    return QVariant::fromValue(this->_log);
}

void LedStrip::updatePorts(void)
{
    QList<QSerialPortInfo> portInfos = QSerialPortInfo::availablePorts();
    QStringList portNames;

    for (int i = 0; i < portInfos.length(); i++) {
        portNames.append(portInfos[i].portName());
    }

    if (portNames != this->_availablePortNames) {
        this->_availablePortNames = portNames;
        emit this->availablePortsChanged();
    }
}

void LedStrip::testLedStripStatus(void)
{
    this->sendCommand("WHAT ARE YOU?");
    QTimer::singleShot(100, this, SLOT(testLedStripConnectionError()));
}

void LedStrip::testLedStripConnectionError()
{
    emit this->ledStripConnectedChanged();
    if (this->_ledStripConnected) {
        this->_ledStripConnected = false;
        return;
    }

    this->log("No LedStrip found", "ERROR");
}

void LedStrip::serialReadyRead()
{
    char c;
    qint64 bytesRead;

    while (0 != (bytesRead = this->_serial.read(&c, 1))) {
        if (-1 == bytesRead) {
            this->log("Error reading serial data", "ERROR");
        }
        if (c == '\n') {
            this->serialBuffer[this->serialBufferPosition] = 0;
            this->serialBufferPosition = 0;
            this->handleSerial();
        } else {
            this->serialBuffer[this->serialBufferPosition] = c;
            if (++this->serialBufferPosition == SERIAL_BUFFER_SIZE) {
                this->serialBufferPosition =  0;
            }
        }
    }
}

void LedStrip::sendCommand(QString command)
{
    command.append('\n');
    this->_serial.write(command.toUtf8());
    this->_serial.flush();
    if (!this->_serial.waitForBytesWritten(1000)) {
        this->log("Timeout while writing to serial", "ERROR");
    }
}

void LedStrip::queryAllLeds()
{
    this->sendCommand("GET ALL");
}

void LedStrip::queryLed(int led)
{
    QByteArray request = QByteArray("GET LED x");
    request[8] = static_cast<char>('0' + led);

    this->sendCommand(request);
}

void LedStrip::log(QString message, QString level)
{
    LedStripLog *logEntry = new LedStripLog();
    logEntry->message = message;
    logEntry->level = level;

    this->_log.append(logEntry);
    emit this->logChanged();
}

void LedStrip::handleSerial()
{
    if (strcmp(this->serialBuffer, "I AM A LEDSTRIP") == 0) {
        this->serialBufferPosition = 0;
        this->_ledStripConnected = true;
        this->queryAllLeds();
        this->log(
            QString("Detected Led Strip on port " + this->_serial.portName()),
            QString("INFO")
        );
    } else if (strncmp(this->serialBuffer, "LED ", 4) == 0) {
        char *ledData = &this->serialBuffer[4];

        do {
            if (ledData[1] == '#') {
                QString color;
                for (int i=1; i<8; i++) {
                    color.append(QChar(ledData[i]));
                }

                this->_ledColors[ledData[0] - '0'] = color;
            }
            ledData = ledData + 9;
        } while (strlen(ledData) > 7);
        emit this->ledColorsChanged();
    }
}
