#ifndef LEDPLUGIN_H
#define LEDPLUGIN_H

#include <QQmlExtensionPlugin>

class LedPlugin: public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "de.kalehmann.LedStrip")

public:
    void registerTypes(const char *uri);
};

#endif // LEDPLUGIN_H
