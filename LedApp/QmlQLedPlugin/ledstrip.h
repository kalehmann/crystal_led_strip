#ifndef LEDSTRIP_H
#define LEDSTRIP_H

#include <ledstriplog.h>
#include <QObject>
#include <QSerialPort>
#include <QVariant>

#define NUM_LEDS 4
#define SERIAL_BUFFER_SIZE 512

enum SerialRequest {NONE, LED_STRIP, ALL_LEDS, SINGLE_LED};

class LedStrip : public QObject
{
    Q_OBJECT

public:
    explicit LedStrip(QObject *parent = nullptr);

    Q_PROPERTY(QStringList availablePorts READ availablePorts NOTIFY availablePortsChanged)
    Q_PROPERTY(QString serialPort READ getSerialPort WRITE setSerialPort NOTIFY serialPortChanged)
    Q_PROPERTY(bool ledStripConnected READ ledStripConnected NOTIFY ledStripConnectedChanged)
    Q_PROPERTY(QStringList ledColors READ getLedColors WRITE setLedColors NOTIFY ledColorsChanged)
    Q_PROPERTY(QVariant log READ getLog() NOTIFY logChanged)

    QStringList availablePorts();
    QString getSerialPort(void);
    void setSerialPort(QString);
    bool ledStripConnected(void);
    QStringList getLedColors();
    void setLedColors(QStringList);
    QVariant getLog();

public slots:
    void updatePorts(void);
    void testLedStripStatus(void);
    void testLedStripConnectionError(void);
    void serialReadyRead(void);

signals:
    void availablePortsChanged();
    void ledStripConnectedChanged();
    void serialPortChanged();
    void ledColorsChanged();
    void logChanged();

protected:
    void sendCommand(QString);
    void queryAllLeds(void);
    void queryLed(int);
    void log(QString message, QString level);
    void handleSerial(void);

    char serialBuffer[SERIAL_BUFFER_SIZE];
    bool _ledStripConnected = false;
    qint64 serialBufferPosition = 0;
    QList<QObject*> _log;
    QSerialPort _serial;
    QStringList _availablePortNames;
    QStringList _ledColors;
};

#endif // LEDSTRIP_H
