#include "ledplugin.h"
#include "ledstrip.h"
#include <qqml.h>

void LedPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<LedStrip>(uri, 1, 0, "LedStrip");
}
