import QtQuick 2.0
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.12

Item {
    id: leds
    property bool flipped: false
    property bool selectable: true
    property int checkedLed: 0
    property list<LedField> ledList
    property list<RadioButton> radioButtons
    property var ledColors: [
        "#000000",
        "#000000",
        "#000000",
        "#000000"
    ]
    width: 480
    height: 100

    Component.onCompleted: {
        ledList.push(led0);
        ledList.push(led1);
        ledList.push(led2);
        ledList.push(led3);

        radioButtons.push(radioLed0);
        radioButtons.push(radioLed1);
        radioButtons.push(radioLed2);
        radioButtons.push(radioLed3);
    }

    onFlippedChanged: {
        if (flipped) {
            led0.x = 360
            led1.x = 240
            led2.x = 120
            led3.x = 0
        } else {
            led0.x = 0
            led1.x = 120
            led2.x = 240
            led3.x = 360
        }
    }

    onSelectableChanged: {
        for (var i=0; i<radioButtons.length; i++) {
            radioButtons[i ].visible = selectable;
        }
    }

    LedField {
        id: led0
        x: 0
        y: 0
        led: "00"
        color: leds.ledColors[0]
        onClicked: radioLed0.checked = true
    }

    RadioButton {
        id: radioLed0
        anchors.top: led0.bottom
        checked: true
        anchors.horizontalCenter: led0.horizontalCenter
        onCheckedChanged: if (checked) leds.checkedLed = 0
    }

    LedField {
        id: led1
        x: 120
        y: 0
        led: "01"
        color: leds.ledColors[1]
        onClicked: radioLed1.checked = true
    }

    RadioButton {
        id: radioLed1
        anchors.top: led1.bottom
        anchors.horizontalCenter: led1.horizontalCenter
        onCheckedChanged: if (checked) leds.checkedLed = 1
    }

    LedField {
        id: led2
        x: 240
        y: 0
        led: "02"
        color: leds.ledColors[2]
        onClicked: radioLed2.checked = true
    }

    RadioButton {
        id: radioLed2
        anchors.top: led2.bottom
        anchors.horizontalCenter: led2.horizontalCenter
        onCheckedChanged: if (checked) leds.checkedLed = 2
    }

    LedField {
        id: led3
        x: 360
        y: 0
        led: "03"
        color: leds.ledColors[3]
        onClicked: radioLed3.checked = true
    }

    RadioButton {
        id: radioLed3
        anchors.top: led3.bottom
        anchors.horizontalCenter: led3.horizontalCenter
        onCheckedChanged: if (checked) leds.checkedLed = 3
    }
}
