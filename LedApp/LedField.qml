import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.12

Item {
    id: ledField
    signal clicked
    property color color: "#000000"
    property string led
    width: 100
    height: 100

    Image {
        id: glow
        source: "pics/crystal_" + ledField.led + "_glow.png"
        anchors.fill: parent
        visible: false
    }

    Image {
        id: outline
        source: "pics/crystal_" + ledField.led + "_outline.png"
        anchors.fill: parent
        z: 1
    }

    ColorOverlay {
        id: colorOverlay
        anchors.fill: glow
        source: glow
        color: ledField.color
        visible: false
    }

    HueSaturation {
        source: colorOverlay
        anchors.fill: glow
        lightness: .3
        saturation: 1
    }

    MouseArea {
        anchors.fill: ledField
        onClicked: ledField.clicked()
    }
}
