import QtQuick 2.0
import QtQuick.Controls 2.5

Item {
    ComboBox {
        id: portSelector
        x: 20
        y: 20
        width: 150
        model: ledStrip.availablePorts
    }

    Button {
        id: btnConnect
        text: "Connect"
        anchors.left: portSelector.right
        anchors.leftMargin: 10
        anchors.top: portSelector.top
        onClicked: {
            ledStrip.serialPort = portSelector.currentText;
            biConnection.running = true
        }
    }

    BusyIndicator {
        id: biConnection
        visible: this.running
        running: false
        anchors.left: btnConnect.right
        anchors.leftMargin: 20
        anchors.verticalCenter: btnConnect.verticalCenter
        Connections {
            target: ledStrip
            onLedStripConnectedChanged: biConnection.running = false
        }
    }

    Rectangle {
        anchors.left:  portSelector.left
        anchors.top: portSelector.bottom
        anchors.topMargin: 20
        width: parent.width - 50
        height: 100
        color: "#eee"
        ListView  {
            id: logList
            width: parent.width
            height: parent.height
            model: ledStrip.log
            clip: true

            ScrollBar.vertical: ScrollBar {
                parent: logList.parent
                anchors.top: logList.top
                anchors.right: logList.left
                anchors.bottom: logList.bottom
            }

            onCountChanged: {
                logList.currentIndex = count - 1
            }

            delegate: Rectangle {
                Text{
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    text: model.modelData.message
                    color: getColor(model.modelData.level)
                }
                color: "transparent"
                height: 20
                width: logList.width

                function getColor(logLevel) {
                    if (logLevel === "ERROR") {
                        return "red";
                    }
                    if (logLevel === "INFO") {
                        return "green";
                    }
                    return "white"
                }
            }
        }
    }
}
