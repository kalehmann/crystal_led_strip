import QtQuick 2.0
import QtQuick.Controls 2.5

Item {
    id: staticPicker
    property var ledColors
    property int checkedLed

    signal connectionChanged

    onConnectionChanged: updateColors()

    onCheckedLedChanged: updateColors()

    function updateColors() {
        colorWrapper.color = ledColors[checkedLed]
        redSlider.value = colorWrapper.color.r * 255
        greenSlider.value = colorWrapper.color.g * 255
        blueSlider.value = colorWrapper.color.b * 255
        hueSlider.value = colorWrapper.color.hsvHue * 255
        saturationSlider.value = colorWrapper.color.hsvSaturation * 255
        valueSlider.value = colorWrapper.color.hsvValue * 255
    }
    Item {
        id: colorWrapper
        property color color
    }

    Text {
        text: qsTr("Red")
        anchors.left: redSlider.left
        anchors.bottom: redSlider.top
    }

    Slider {
        id: redSlider
        x: 20
        y: 50
        width: 150
        from: 0
        to: 255
        onValueChanged: {
            colorWrapper.color = staticPicker.ledColors[staticPicker.checkedLed]
            colorWrapper.color.r = value / 255
            staticPicker.ledColors[staticPicker.checkedLed] = colorWrapper.color
        }
    }

    Text {
        text: qsTr("Green")
        anchors.left: redSlider.left
        anchors.bottom: greenSlider.top
    }

    Slider {
        id: greenSlider
        anchors.top: redSlider.bottom
        anchors.topMargin: 30
        anchors.left: redSlider.left
        width: redSlider.width
        from: 0
        to: 255
        onValueChanged: {
            colorWrapper.color = staticPicker.ledColors[staticPicker.checkedLed]
            colorWrapper.color.g = value / 255
            staticPicker.ledColors[staticPicker.checkedLed] = colorWrapper.color
        }
    }

    Text {
        text: qsTr("Blue")
        anchors.left: redSlider.left
        anchors.bottom: blueSlider.top
    }

    Slider {
        id: blueSlider
        anchors.top: greenSlider.bottom
        anchors.topMargin: 30
        anchors.left: redSlider.left
        width: redSlider.width
        from: 0
        to: 255
        onValueChanged: {
            colorWrapper.color = staticPicker.ledColors[staticPicker.checkedLed]
            colorWrapper.color.b = value / 255
            staticPicker.ledColors[staticPicker.checkedLed] = colorWrapper.color
        }
    }

    Text {
        text: qsTr("Hue")
        anchors.left: hueSlider.left
        anchors.bottom: hueSlider.top
    }

    Slider {
        id: hueSlider
        anchors.left: redSlider.right
        anchors.leftMargin: 20
        anchors.verticalCenter: redSlider.verticalCenter
        width: redSlider.width
        from: 0
        to: 255
        onValueChanged: {
            colorWrapper.color = staticPicker.ledColors[staticPicker.checkedLed]
            colorWrapper.color.hsvHue = value / 255
            staticPicker.ledColors[staticPicker.checkedLed] = colorWrapper.color
        }
    }

    Text {
        text: qsTr("Saturation")
        anchors.left: hueSlider.left
        anchors.bottom: saturationSlider.top
    }

    Slider {
        id: saturationSlider
        anchors.top: hueSlider.bottom
        anchors.topMargin: 30
        anchors.left: hueSlider.left
        width: redSlider.width
        from: 0
        to: 255
        onValueChanged: {
            colorWrapper.color = staticPicker.ledColors[staticPicker.checkedLed]
            colorWrapper.color.hsvSaturation = value / 255
            staticPicker.ledColors[staticPicker.checkedLed] = colorWrapper.color
        }
    }

    Text {
        text: qsTr("Value")
        anchors.left: hueSlider.left
        anchors.bottom: valueSlider.top
    }

    Slider {
        id: valueSlider
        y: 0
        anchors.top: saturationSlider.bottom
        anchors.topMargin: 30
        anchors.left: hueSlider.left
        width: redSlider.width
        from: 0
        to: 255
        onValueChanged: {
            colorWrapper.color = staticPicker.ledColors[staticPicker.checkedLed]
            colorWrapper.color.hsvValue = value / 255
            staticPicker.ledColors[staticPicker.checkedLed] = colorWrapper.color
        }
    }

}
