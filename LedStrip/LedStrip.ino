#include <FastLED.h>
#include <string.h>

#define LED1_PIN 7
#define LED2_PIN 5

#define BUFFER_SIZE 32
char serialBuffer[BUFFER_SIZE];
int bytesRead = 0;

CRGB leds[4];

void setup()
{
        FastLED.addLeds<WS2812,LED1_PIN>(&leds[0], 1);
        FastLED.addLeds<WS2812,LED2_PIN>(&leds[1], 3);

        Serial.begin(9600, SERIAL_8N1);

        leds[0] = CRGB(0, 10, 10);
        leds[1] = CRGB(100, 0, 0);
        leds[2] = CRGB(0, 100, 0);
        leds[3] = CRGB(0, 0, 100);
        FastLED.show();
}

void setLed(unsigned char led, unsigned char r, unsigned char b, unsigned char g)
{
        leds[led] = CRGB(r, g, b);
        FastLED.show();
}

unsigned char hexToNibble(char hex)
{
        if (hex < 0x30 || hex > 0x46 || (hex > 0x39 && hex < 0x41)) {
                // Hex out of range
                return 0;        
        }

        if (hex < 0x3a) {
                return hex - '0';        
        }

        return hex - 0x37;
}

unsigned char hexToChar(const char *buffer)
{
        return (hexToNibble(buffer[0]) << 4) | hexToNibble(buffer[1]);
}

bool isValidHex(const char *hex, size_t n)
{
        for (size_t i = 0; i < n; i++) {
                if (hex[i] < 0x30 || hex[i] > 0x46 || (hex[i] > 0x39 && hex[i] < 0x41)) {
                        // Hex out of range
                        return false;        
                }    
     }

     return true;
}

void hexTripletToRGB(const char *hex, unsigned char *r, unsigned char *g, unsigned char *b)
{
        *r = hexToChar(hex);
        *g = hexToChar(hex + 2);
        *b = hexToChar(hex + 4);
}

char nibbleToHex(unsigned char nibble)
{
        if (nibble > 0xf) {
                // Out of range
                return 0;        
        }

        if (nibble < 10) {
                return '0' + nibble;        
        }

        return 0x37 + nibble;
}

void charToHex(char *buffer, char i)
{
        unsigned char upper = i >> 4;
        unsigned char lower = i & 0xf;

        buffer[0] = nibbleToHex(upper);
        buffer[1] = nibbleToHex(lower);
}

void rgbToHexTriplet(char *buffer, char r, char g, char b)
{
        buffer[0] = '#';
        charToHex(buffer + 1, r);
        charToHex(buffer + 3, g);
        charToHex(buffer + 5, b);       
}

void evalBuffer()
{
        if (0 == strcmp(serialBuffer, "WHAT ARE YOU?")) {
                Serial.print("I AM A LEDSTRIP\n");
                return;        
        }

        if (0 == strncmp(serialBuffer, "GET ALL", 7)) {
                Serial.print("LED");
                for (int led=0; led<4; led++) {
                        CRGB crgb = leds[led];
                        Serial.print(" ");
                        Serial.print(led);
                        char buffer[8];
                        rgbToHexTriplet(buffer, crgb.r, crgb.g, crgb.b);
                        buffer[7] = 0;
                        Serial.print(buffer);
                }
                Serial.print('\n');
                                
                return;
        }

        if (0 == strncmp(serialBuffer, "GET LED ", 8)) {
                int led = serialBuffer[8] - '0';

                if (led < 0 || led > 3) {
                        // Led out of range
                        return;        
                }

                CRGB crgb = leds[led];
                char buffer[13];
                memcpy(buffer, "LED ", 4);
                buffer[4] = led + '0';
                rgbToHexTriplet(&buffer[5], crgb.r, crgb.g, crgb.b);
                buffer[12] = 0;
                Serial.print(buffer);
                Serial.print('\n');
                return;
        }

        if (0 == strncmp(serialBuffer, "SET LED ", 8) && 16 == strlen(serialBuffer)) {
                int led = serialBuffer[8] - '0';

                if (led < 0 || led > 3) {
                        // Led out of range
                        return;        
                }
                if (serialBuffer[9] != '#') {
                        return;
                }

                if (!isValidHex(&serialBuffer[10], 6)) {
                        return;        
                }

                unsigned char r, g, b;
                hexTripletToRGB(&serialBuffer[10], &r, &g, &b);

                leds[led] = CRGB(r, g, b);
                FastLED.show();
                
                return;
        }
}


void serialEvent()
{
        char incomingByte;
        while (Serial.available()) {
                incomingByte = Serial.read();
                if (incomingByte == '\n') {
                        serialBuffer[bytesRead] = 0;
                        evalBuffer();
                        bytesRead = 0; 
                } else {
                        serialBuffer[bytesRead] = incomingByte;
                        if (++bytesRead == BUFFER_SIZE) {
                                bytesRead = 0;
                        }
                }
        }
}

void loop()
{
        // I am useless
}
